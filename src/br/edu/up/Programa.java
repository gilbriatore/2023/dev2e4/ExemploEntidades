package br.edu.up;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.Scanner;

import br.edu.up.entidades.Produto;

public class Programa {
	
	public static void main(String[] args) throws FileNotFoundException {
		
		String arquivoOrigem = "C:\\_ws\\dev2e4\\ws\\ExemploEntidades\\src\\produtos.csv";
		File arquivo = new File(arquivoOrigem);
		Scanner leitor = new Scanner(arquivo);
		
		String arquivoDestino = "C:\\_ws\\dev2e4\\ws\\ExemploEntidades\\src\\relatorio.txt";
		Formatter gravador = new Formatter(arquivoDestino);
		
		Produto[] vetorDeProdutos = new Produto[2];
		
		int index = 0;
		while(leitor.hasNextLine()) {
			String linha = leitor.nextLine();
			String[] valores = linha.split(";");
			
			Produto produto = new Produto();
			produto.codigo = Integer.parseInt(valores[0]);
			produto.nome = valores[1];
			produto.marca = valores[2];
			produto.preco = Double.parseDouble(valores[3]);
			
			vetorDeProdutos[index] = produto;
			index++;			
		}
		
		int qtdeDeProdutos = 0;
		double totalDosProdutos = 0;
		
		for (int i = 0; i < vetorDeProdutos.length; i++) {
			Produto produto = vetorDeProdutos[i];
			System.out.println("Código: " + produto.codigo);
			System.out.println("Nome: " + produto.nome);
			System.out.println("Marca: " + produto.marca);
			System.out.println("Preço: " + produto.preco);
			System.out.println();
			
			gravador.format("Código: " + produto.codigo + "\n");
			gravador.format("Nome: " + produto.nome + "\n");
			gravador.format("Marca: " + produto.marca + "\n");
			gravador.format("Preço: " + produto.preco + "\n\n");
			
			
			qtdeDeProdutos++;
			totalDosProdutos += produto.preco;
		}
		
		System.out.println("Quantidade: " + qtdeDeProdutos);
		System.out.println("Total: " + totalDosProdutos);
		
		gravador.format("Quantidade: " + qtdeDeProdutos + "\n");
		gravador.format("Total: " + totalDosProdutos);
		
		
		leitor.close();
		gravador.close();
	}
	
//	public static void main(String[] args) {
//
//		Produto meuProduto = new Produto();
//		meuProduto.codigo = 1;
//		meuProduto.nome = "Computador";
//		meuProduto.marca = "Dell";
//		meuProduto.preco = 3000.00;
//		
//		Produto seuProduto = new Produto();
//		seuProduto.codigo = 2;
//		seuProduto.nome = "iPhone";
//		seuProduto.marca = "Apple";
//		seuProduto.preco = 5000.00;
//		
//		System.out.println("Meu produto");
//		System.out.println("Código: " + meuProduto.codigo);
//		System.out.println("Nome: " + meuProduto.nome);
//		System.out.println("Marca: " + meuProduto.marca);
//		System.out.println("Preço: " + meuProduto.preco);
//		System.out.println();
//		
//		System.out.println("Seu produto");
//		System.out.println("Código: " + seuProduto.codigo);
//		System.out.println("Nome: " + seuProduto.nome);
//		System.out.println("Marca: " + seuProduto.marca);
//		System.out.println("Preço: " + seuProduto.preco);
//		System.out.println();
//		
//	}

}
